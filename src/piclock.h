/*******************************************************************************
 * piclock
 *
 * video.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PICLOCK_H
#define PICLOCK_H

#include <gtk/gtk.h>

#define KEYSYMS_F     "/etc/pibox-keysyms"
#define KEYSYMS_FD    "data/pibox-keysyms"

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef PICLOCK_C
int     daemonEnabled = 0;
char    errBuf[256];
gchar   *imagePath = NULL;
#else
extern int      daemonEnabled;
extern char     errBuf[];
extern gchar   *imagePath;
#endif /* PICLOCK_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "piclock"
#define MAXBUF      4096

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include "cli.h"
#include "utils.h"

#endif /* !PICLOCK_H */

