/*******************************************************************************
 * piclock
 *
 * piclock.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PICLOCK_C

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pthread.h>
#include <errno.h>
#include <pibox/utils.h>
#include <pibox/log.h>
#include <pibox/parson.h>

#include "piclockwidget.h" 
#include "piclock.h" 

GtkWidget *window;
GtkWidget *calendar;
GtkWidget *hour;
GtkWidget *minute;
GtkWidget *pm;
GtkWidget *piclock;
GtkWidget *darea;
cairo_t *cr = NULL;

PICLOCK_THEME_T *piclock_theme = NULL;

static pthread_mutex_t timeMutex = PTHREAD_MUTEX_INITIALIZER;
struct tm tms;

// The home key is the key that exits the app.
guint homeKey = -1;

/*
 *========================================================================
 * Name:   loadTheme
 * Prototype:  void loadTheme( void )
 *
 * Description:
 * Read the theme configuration files, first the current theme name then
 * that theme's configuration.
 *
 * Notes:
 * A theme configuration file is JSON formatted and looks like this:
 * {
 *      theme: {
 *          face:"<Name of clock face file>",
 *          overlay:"<Name of clock overlay file>",
 *          hour:"<Name of hour hand file>",
 *          min:"<Name of minute hand file>",
 *          sec:"<Name of second hand file>",
 *          center:{
 *              hour: { x:n, y:m },
 *              min: { x:n, y:m },
 *              sec: { x:n, y:m }
 *          },
 *      }
 * }
 *
 * All fields are required.  If you don't have an overlay or a face you need
 * to create a transparent image of the correct size.
 *
 * Maximum size for face: 512 x 512
 * Center points are the location in the image file around which rotation
 * should occur.
 * Center points must lie within hxw of clock hand files.
 *========================================================================
 */
void 
loadTheme( void )
{
    FILE            *fd;
    char            *buf;
    char            themeName[33];
    const char      *face;
    const char      *hour;
    const char      *min;
    const char      *sec;
    const char      *overlay;
    gint 			image_width, image_height;
    gdouble         x, y;
    struct stat     stat_buf;
    JSON_Value      *data;
    JSON_Object     *obj;

    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(cliOptions.themeConf)+2);
    sprintf(buf, "%s/%s", cliOptions.themeDir,cliOptions.themeConf);
    if ( stat(buf, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Missing theme configuration file. Using default no-theme.\n");
        free(buf);
        return;
    }
    fd = fopen(buf, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't open current theme file. Using default no-theme.\n");
        free(buf);
        return;
    }
    free(buf);

    /* Theme names are limited to 32 characters */
    memset(themeName, 0, 33);
    fgets(themeName, 32, fd);
    piboxStripNewline(themeName);
    fclose(fd);

    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+2);
    sprintf(buf, "%s/%s", cliOptions.themeDir,themeName);
    if ( stat(buf, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Missing theme directory (%s). Using default no-theme.\n", buf);
        free(buf);
        return;
    }
    free(buf);
    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(cliOptions.themeConf)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,cliOptions.themeConf);
    if ( stat(buf, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Missing conf file (%s) in theme directory. Using default no-theme.\n", buf);
        free(buf);
        return;
    }

    /* Slurp the file into JSON */
    piboxLogger(LOG_INFO, "theme file: %s\n", buf);
    data = json_parse_file_with_comments(buf);
    if ( data == NULL )
    {
        piboxLogger(LOG_ERROR, "JSON parse error reading conf file (%s) in theme directory. Using default no-theme.\n", 
                buf);
        free(buf);
        return;
    }
    free(buf);

    /* Dump the contents */
    obj     = json_value_get_object(data);
    face    = json_object_dotget_string(obj, "theme.face");
    hour    = json_object_dotget_string(obj, "theme.hour");
    min     = json_object_dotget_string(obj, "theme.min");
    sec     = json_object_dotget_string(obj, "theme.sec");
    overlay = json_object_dotget_string(obj, "theme.overlay");

    piboxLogger(LOG_INFO, "Face   : %s\n", face);
    piboxLogger(LOG_INFO, "Hour   : %s\n", hour);
    piboxLogger(LOG_INFO, "Min    : %s\n", min);
    piboxLogger(LOG_INFO, "Sec    : %s\n", sec);
    piboxLogger(LOG_INFO, "overlay: %s\n", overlay);
    piboxLogger(LOG_INFO, "hour xy: %f %f\n", 
            json_object_dotget_number(obj, "theme.center.hour.x"),
            json_object_dotget_number(obj, "theme.center.hour.y")
            );
    piboxLogger(LOG_INFO, "min xy : %f %f\n", 
            json_object_dotget_number(obj, "theme.center.min.x"),
            json_object_dotget_number(obj, "theme.center.min.y")
            );
    piboxLogger(LOG_INFO, "sec xy : %f %f\n", 
            json_object_dotget_number(obj, "theme.center.sec.x"),
            json_object_dotget_number(obj, "theme.center.sec.y")
            );

    /* Test for all files. */
    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(face)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,face);
    if ( stat(buf, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Missing face file (%s) in theme directory. Using default no-theme.\n", buf);
        free(buf);
        json_value_free(data);
        return;
    }
    free(buf);
    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(overlay)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,overlay);
    if ( stat(buf, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Missing overlay file (%s) in theme directory. Using default no-theme.\n", buf);
        free(buf);
        json_value_free(data);
        return;
    }
    free(buf);
    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(hour)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,hour);
    if ( stat(buf, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Missing hour file (%s) in theme directory. Using default no-theme.\n", buf);
        free(buf);
        json_value_free(data);
        return;
    }
    free(buf);
    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(min)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,min);
    if ( stat(buf, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Missing minute file (%s) in theme directory. Using default no-theme.\n", buf);
        free(buf);
        json_value_free(data);
        return;
    }
    free(buf);
    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(sec)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,sec);
    if ( stat(buf, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Missing second file (%s) in theme directory. Using default no-theme.\n", buf);
        free(buf);
        json_value_free(data);
        return;
    }

    /* 
     * Create the GdkPixbuf instances for each image, so we can reuse them without having to re-read them.
     * Also, validate the configurations (center point, max size, etc.)
     */
    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(face)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,face);
    cliOptions.face = gdk_pixbuf_new_from_file(buf, NULL);
    image_width = gdk_pixbuf_get_width(cliOptions.face);
    image_height = gdk_pixbuf_get_height(cliOptions.face);
    free(buf);
    if ( (image_width > 512) || (image_height > 512) )
    {
        piboxLogger(LOG_ERROR, "Invalid clock face size (%d x %d). Using default no-theme.\n",
                image_width, image_height);
        g_object_unref(cliOptions.face);
        cliOptions.face = NULL;
        cliOptions.face = NULL;
        json_value_free(data);
        return;
    }

    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(overlay)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,overlay);
    cliOptions.overlay = gdk_pixbuf_new_from_file(buf, NULL);
    image_width = gdk_pixbuf_get_width(cliOptions.overlay);
    image_height = gdk_pixbuf_get_height(cliOptions.overlay);
    free(buf);
    if ( (image_width > 512) || (image_height > 512) )
    {
        piboxLogger(LOG_ERROR, "Invalid overlay size (%d x %d). Using default no-theme.\n",
                image_width, image_height);
        g_object_unref(cliOptions.face);
        g_object_unref(cliOptions.overlay);
        cliOptions.face = NULL;
        cliOptions.overlay = NULL;
        json_value_free(data);
        return;
    }

    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(hour)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,hour);
    cliOptions.hour = gdk_pixbuf_new_from_file(buf, NULL);
    image_width = gdk_pixbuf_get_width(cliOptions.hour);
    image_height = gdk_pixbuf_get_height(cliOptions.hour);
    free(buf);
    x = json_object_dotget_number(obj, "theme.center.hour.x");
    y = json_object_dotget_number(obj, "theme.center.hour.y");
    if ( (x<0) || (x > image_width) || (y<0) || (y > image_height) )
    {
        piboxLogger(LOG_ERROR, "Hour hand center (%fx%f) outside of image (%dx%d). Using default no-theme.\n",
                x,y,image_width,image_height);
        g_object_unref(cliOptions.face);
        g_object_unref(cliOptions.overlay);
        g_object_unref(cliOptions.hour);
        cliOptions.face = NULL;
        cliOptions.overlay = NULL;
        cliOptions.hour = NULL;
        json_value_free(data);
        return;
    }

    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(min)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,min);
    cliOptions.min = gdk_pixbuf_new_from_file(buf, NULL);
    image_width = gdk_pixbuf_get_width(cliOptions.min);
    image_height = gdk_pixbuf_get_height(cliOptions.min);
    free(buf);
    x = json_object_dotget_number(obj, "theme.center.min.x");
    y = json_object_dotget_number(obj, "theme.center.min.y");
    if ( (x<0) || (x > image_width) || (y<0) || (y > image_height) )
    {
        piboxLogger(LOG_ERROR, "Minute hand center (%fx%f) outside of image (%dx%d). Using default no-theme.\n",
                x,y,image_width,image_height);
        g_object_unref(cliOptions.face);
        g_object_unref(cliOptions.overlay);
        g_object_unref(cliOptions.hour);
        g_object_unref(cliOptions.min);
        cliOptions.face = NULL;
        cliOptions.overlay = NULL;
        cliOptions.hour = NULL;
        cliOptions.min  = NULL;
        json_value_free(data);
        return;
    }

    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(sec)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,sec);
    cliOptions.sec = gdk_pixbuf_new_from_file(buf, NULL);
    image_width = gdk_pixbuf_get_width(cliOptions.sec);
    image_height = gdk_pixbuf_get_height(cliOptions.sec);
    free(buf);
    x = json_object_dotget_number(obj, "theme.center.sec.x");
    y = json_object_dotget_number(obj, "theme.center.sec.y");
    if ( (x<0) || (x > image_width) || (y<0) || (y > image_height) )
    {
        piboxLogger(LOG_ERROR, 
                "Second hand center (%fx%f) outside of image (%dx%d). Using default no-theme.\n",
                x,y,image_width,image_height);
        g_object_unref(cliOptions.face);
        g_object_unref(cliOptions.overlay);
        g_object_unref(cliOptions.hour);
        g_object_unref(cliOptions.min);
        g_object_unref(cliOptions.sec);
        cliOptions.face = NULL;
        cliOptions.overlay = NULL;
        cliOptions.hour = NULL;
        cliOptions.min  = NULL;
        cliOptions.sec  = NULL;
        json_value_free(data);
        return;
    }

    /* Everything is validated.  Create a structure we can pass to the widget. */

    /*
     * Make sure use of these surfaces doesn't destroy them.
     */
    g_object_ref(cliOptions.face);
    g_object_ref(cliOptions.overlay);
    g_object_ref(cliOptions.hour);
    g_object_ref(cliOptions.min);
    g_object_ref(cliOptions.sec);

    /* Save the parsed theme data. */
    cliOptions.theme = data;

    piclock_theme = (PICLOCK_THEME_T *)calloc(1, sizeof(PICLOCK_THEME_T));

    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(face)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,face);
    piclock_theme->face         = g_strdup(buf);

    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(overlay)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,overlay);
    piclock_theme->overlay      = g_strdup(buf);

    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(hour)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,hour);
    piclock_theme->hour         = g_strdup(buf);

    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(min)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,min);
    piclock_theme->min          = g_strdup(buf);

    buf = (char *)calloc(1, strlen(cliOptions.themeDir)+strlen(themeName)+strlen(sec)+3);
    sprintf(buf, "%s/%s/%s", cliOptions.themeDir,themeName,sec);
    piclock_theme->sec          = g_strdup(buf);

    piclock_theme->hour_pt.x    = json_object_dotget_number(obj, "theme.center.hour.x");
    piclock_theme->hour_pt.y    = json_object_dotget_number(obj, "theme.center.hour.y");
    piclock_theme->min_pt.x     = json_object_dotget_number(obj, "theme.center.min.x");
    piclock_theme->min_pt.y     = json_object_dotget_number(obj, "theme.center.min.y");
    piclock_theme->sec_pt.x     = json_object_dotget_number(obj, "theme.center.sec.x");
    piclock_theme->sec_pt.y     = json_object_dotget_number(obj, "theme.center.sec.y");
    piclock_theme->face_sf      = cliOptions.face;
    piclock_theme->overlay_sf   = cliOptions.overlay;
    piclock_theme->hour_sf      = cliOptions.hour;
    piclock_theme->min_sf       = cliOptions.min;
    piclock_theme->sec_sf       = cliOptions.sec;
}

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void 
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    // Read in /etc/pibox-keysysm
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        // Ignore comments
        if ( buf[0] == '#' )
            continue;

        // Strip leading white space
        ptr = buf;
        ptr = trim(ptr);

        // Ignore blank lines
        if ( strlen(ptr) == 0 )
            continue;

        // Strip newline
        piboxStripNewline(ptr);

        // Grab first token
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        // Grab second token
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        // Set the home key
        if ( strncasecmp("home", action, 4) == 0 )
        {
            piboxLogger(LOG_INFO, "keysym = %s\n", keysym);
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   time_cb
 * Prototype:  gboolean time_cb( GtkWidget *widget, gpointer )
 *
 * Description:
 * Update the analog clock and set the time when user changes date/time settings.
 *========================================================================
 */
static gboolean 
time_cb(GtkWidget *widget, gpointer user_data)
{      
    gint hourval;
    gint minval;
    int state;
    guint year;
    guint month;
    guint day;

    gtk_calendar_get_date(GTK_CALENDAR(calendar), &year, &month, &day);
    state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pm));
    hourval = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(hour));
    minval = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(minute));
    if ( !state )
        gtk_piclock_set_time(GTK_PICLOCK(piclock), hourval, minval);
    else
        gtk_piclock_set_time(GTK_PICLOCK(piclock), hourval+12, minval);
    gtk_widget_queue_draw(GTK_WIDGET(piclock));

    // Save the updated time - a g-timer will do that actual update.
    pthread_mutex_lock( &timeMutex );
    tms.tm_sec = 0;
    tms.tm_min = minval;
    if ( !state )
        tms.tm_hour = hourval;
    else
        tms.tm_hour = hourval+12;
    tms.tm_mday = day;
    tms.tm_mon = month;
    tms.tm_year = year-1900;
    pthread_mutex_unlock( &timeMutex );
    return(TRUE);
}

/*
 *========================================================================
 * Name:   updateSystemTime
 * Prototype:  void updateSystemTime( void )
 *
 * Description:
 * Update the system time.  This is done right before exiting and outside
 * the GTK main loop to avoid problems with callback timings.
 *========================================================================
 */
void updateSystemTime ()
{
    struct timeval tval;
    struct timezone tz;
    int rc;

    pthread_mutex_lock( &timeMutex );

    // If time_cb() updated the tms struct then we need to update the system time.
    if ( tms.tm_sec != -1 )
    {
        tval.tv_sec = mktime(&tms);
        tval.tv_usec = 0;
        rc = settimeofday( &tval, &tz );
        if ( rc == -1 )
            piboxLogger(LOG_ERROR, "Error setting time: %s\n", strerror(errno));
        piboxLogger(LOG_INFO, "%02d/%02d/%4d %02d/%02d\n", tms.tm_mon, tms.tm_mday, 1900+tms.tm_year, tms.tm_hour, tms.tm_min);
    }

    pthread_mutex_unlock( &timeMutex );
}

/*
 *========================================================================
 * Name:   updateClock
 * Prototype:  gboolean updateClock( gpointer )
 *
 * Description:
 * Called once a second from a glib timer to cause the analog clock to be
 * updated.
 *========================================================================
 */
gboolean updateClock (gpointer data)
{
    gtk_piclock_tick(GTK_PICLOCK(data));
    gtk_widget_queue_draw(GTK_WIDGET(data));
    return TRUE;
}

/*
 *========================================================================
 * Name:   key_press_calendar
 * Prototype:  void key_press_calendar( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles moving month/year using arrow keys.
 *========================================================================
 */
static gboolean 
key_press_calendar(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{      
    guint year;
    guint month;
    guint day;

    switch(event->keyval) 
    {
        // Move month down
        case GDK_KEY_Left:
        case GDK_KEY_KP_Left:
            if (event->state & GDK_CONTROL_MASK)
            {
                gtk_calendar_get_date(GTK_CALENDAR(widget), &year, &month, &day);
                if ( month == 0 )
                {
                    year--;
                    month = 11;
                }
                else
                    month--;
                gtk_calendar_select_month(GTK_CALENDAR(widget), month, year);
                time_cb(NULL, NULL);
                return(TRUE);
            }
            break;

        // Move month up
        case GDK_KEY_Right:
        case GDK_KEY_KP_Right:
            if (event->state & GDK_CONTROL_MASK)
            {
                gtk_calendar_get_date(GTK_CALENDAR(widget), &year, &month, &day);
                if ( month == 11 )
                {
                    year++;
                    month = 0;
                }
                else
                    month++;
                gtk_calendar_select_month(GTK_CALENDAR(widget), month, year);
                time_cb(NULL, NULL);
                return(TRUE);
            }
            break;

        // Move year up
        case GDK_KEY_Up:
        case GDK_KEY_KP_Up:
            if (event->state & GDK_CONTROL_MASK)
            {
                gtk_calendar_get_date(GTK_CALENDAR(widget), &year, &month, &day);
                year++;
                gtk_calendar_select_month(GTK_CALENDAR(widget), month, year);
                time_cb(NULL, NULL);
                return(TRUE);
            }
            break;

        // Move year down
        case GDK_KEY_Down:
        case GDK_KEY_KP_Down:
            if (event->state & GDK_CONTROL_MASK)
            {
                gtk_calendar_get_date(GTK_CALENDAR(widget), &year, &month, &day);
                year--;
                gtk_calendar_select_month(GTK_CALENDAR(widget), month, year);
                time_cb(NULL, NULL);
                return(TRUE);
            }
            break;

        default:
            break;
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   key_press_calendar_after
 * Prototype:  boolean key_press_calendar_after( GtkWidget *, gpointer )
 *
 * Description:
 * When the day of month changes, we need to update the tms structure.
 *========================================================================
 */
static gboolean 
key_press_calendar_after(GtkWidget *widget, gpointer user_data)
{      
    time_cb(NULL, NULL);
    return(TRUE);
}

/*
 *========================================================================
 * Name:   home_cb
 * Prototype:  boolean home_cb( GtkWidget *, gpointer )
 *
 * Description:
 * When the day of month changes, we need to update the tms structure.
 *========================================================================
 */
static gboolean 
home_cb(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{      
    switch(event->keyval) 
    {
        case GDK_KEY_space:
            gtk_main_quit();
            return(TRUE);
            break;

        default:
            break;
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles exiting the application via keystrokes.
 *========================================================================
 */
static gboolean 
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{      
    switch(event->keyval) 
    {
        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                gtk_main_quit();
                return(TRUE);
            }
            break;

        case GDK_KEY_Home:
            piboxLogger(LOG_INFO, "Home key\n");
            gtk_main_quit();
            return(TRUE);
            break;

        default:
            if ( event->keyval == homeKey )
            {   
                piboxLogger(LOG_INFO, "Keysym configured home key\n");
                gtk_main_quit();
                return(TRUE);
            }
            break;
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a analog clock on the left and calendar on
 * the right.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget           *hbox1;
    GtkWidget           *hbox2;
    GtkWidget           *hbox3;
    GtkWidget           *vbox1;
    GtkWidget           *label;
    GtkWidget           *home;
    time_t              now;
    struct tm           *timespec;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);

    // Two columns: analog clock and calendar.
    hbox1 = gtk_table_new (1, 2, TRUE);
    gtk_widget_set_name (hbox1, "hbox1");
    gtk_widget_show (hbox1);
    gtk_container_add (GTK_CONTAINER (window), hbox1);

    /*
     * ---------------------------------------
     * The clock on the left side
     * ---------------------------------------
     */
    vbox1 = gtk_vbox_new (FALSE, 20);
    gtk_widget_set_name (hbox1, "vbox1");
    gtk_widget_show (vbox1);
    gtk_table_attach (GTK_TABLE (hbox1), vbox1, 0, 1, 0, 1, GTK_EXPAND|GTK_FILL, GTK_EXPAND|GTK_FILL, 0, 0);

    piclock = gtk_piclock_new();
    gtk_piclock_set_theme(GTK_PICLOCK(piclock), piclock_theme);
    gtk_widget_show (piclock);
    gtk_box_pack_start (GTK_BOX (vbox1), piclock, TRUE, TRUE, 0);

    /*
     * ---------------------------------------
     * Calendar
     * ---------------------------------------
     */
    vbox1 = gtk_vbox_new (FALSE, 20);
    gtk_widget_set_name (hbox1, "vbox1");
    gtk_widget_show (vbox1);
    gtk_table_attach (GTK_TABLE (hbox1), vbox1, 1, 2, 0, 1, 0, 0, 60, 0);

    // Spacer
    label = gtk_label_new(NULL);
    gtk_widget_show (label);
    gtk_box_pack_start (GTK_BOX (vbox1), label, TRUE, TRUE, 0);

    // Year, month, day
    now = time(NULL);
    timespec = localtime( &now );
    piboxLogger(LOG_INFO, "Current time: %s\n", asctime( localtime(&now) ) );
    calendar = gtk_calendar_new();
    gtk_calendar_select_month(GTK_CALENDAR(calendar), timespec->tm_mon, 1900+timespec->tm_year);
    gtk_calendar_select_day(GTK_CALENDAR(calendar), timespec->tm_mday);
    gtk_widget_show (calendar);
    gtk_box_pack_start (GTK_BOX (vbox1), calendar, FALSE, FALSE, 20);
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(calendar),
                "key_press_event",
                G_CALLBACK(key_press_calendar),
                NULL);
    g_signal_connect_after(G_OBJECT(calendar),
                "day-selected",
                G_CALLBACK(key_press_calendar_after),
                NULL);

    // Hour, Minute, AM/PM
    hbox2 = gtk_hbox_new (FALSE, 0);
    gtk_widget_set_name (hbox2, "hbox2");
    gtk_widget_show (hbox2);
    gtk_box_pack_start (GTK_BOX (vbox1), hbox2, FALSE, FALSE, 0);

    hour = gtk_spin_button_new_with_range(0, 11, 1);
    if ( timespec->tm_hour > 11 )
        gtk_spin_button_set_value(GTK_SPIN_BUTTON(hour), timespec->tm_hour-12);
    else
        gtk_spin_button_set_value(GTK_SPIN_BUTTON(hour), timespec->tm_hour);
    gtk_widget_show (hour);
    gtk_box_pack_start (GTK_BOX (hbox2), hour, FALSE, TRUE, 10);
    g_signal_connect(G_OBJECT(hour),
                "value-changed",
                G_CALLBACK(time_cb),
                NULL);

    label = gtk_label_new("Hour");
    gtk_widget_show (label);
    gtk_box_pack_start (GTK_BOX (hbox2), label, TRUE, TRUE, 10);

    minute = gtk_spin_button_new_with_range(0, 59, 1);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(minute), timespec->tm_min);
    gtk_widget_show (minute);
    gtk_box_pack_start (GTK_BOX (hbox2), minute, FALSE, TRUE, 10);
    g_signal_connect(G_OBJECT(minute),
                "value-changed",
                G_CALLBACK(time_cb),
                NULL);

    label = gtk_label_new("Minute");
    gtk_widget_show (label);
    gtk_box_pack_start (GTK_BOX (hbox2), label, TRUE, TRUE, 10);

    pm = gtk_check_button_new_with_label("PM");
    gtk_widget_show (pm);
    gtk_box_pack_start (GTK_BOX (hbox2), pm, FALSE, FALSE, 10);
    if ( timespec->tm_hour > 11 )
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(pm), TRUE);
    g_signal_connect(G_OBJECT(pm),
                "toggled",
                G_CALLBACK(time_cb),
                NULL);

    // Button bar
    hbox3 = gtk_hbox_new (FALSE, 0);
    gtk_widget_set_name (hbox3, "hbox3");
    gtk_widget_show (hbox3);
    gtk_box_pack_start (GTK_BOX (vbox1), hbox3, FALSE, FALSE, 0);

    home = gtk_button_new_from_stock ("gtk-home");
    gtk_button_set_relief (GTK_BUTTON(home), GTK_RELIEF_NONE);
    gtk_widget_show (home);
    gtk_box_pack_start (GTK_BOX (hbox3), home, TRUE, TRUE, 0);
    g_signal_connect ((gpointer) home, 
                "key_press_event",
                G_CALLBACK(home_cb),
                NULL);

    // Spacer (bottom of right column)
    label = gtk_label_new(NULL);
    gtk_widget_show (label);
    gtk_box_pack_start (GTK_BOX (vbox1), label, TRUE, TRUE, 0);

    // Make the main window die when destroy is issued.
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    // Now position the window and set its title
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 768, 768); 
    gtk_window_set_title(GTK_WINDOW(window), "PiClock");

    return window;
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int 
main(int argc, char *argv[])
{
    GtkWidget *window;
    struct stat stat_buf;
    char path[MAXBUF];

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);
    tms.tm_sec = -1;

    // Setup logging
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    
    // Get keyboard behaviours for environment.
    loadKeysyms();

    piboxLogger(LOG_INFO, "Running from: %s\n", getcwd(path, MAXBUF));
    piboxLogger(LOG_INFO, "Themedir: %s\n", cliOptions.themeDir);
    piboxLogger(LOG_INFO, "Themeconf: %s\n", cliOptions.themeConf);

    /* Load the theme file, if any. */
    loadTheme();

    gtk_init(&argc, &argv);

    if ( isCLIFlagSet( CLI_TEST) && (stat("data/gtkrc", &stat_buf) == 0) )
        gtk_rc_parse("data/gtkrc");

    window = createWindow();
    gtk_window_fullscreen (GTK_WINDOW(window));
    gtk_widget_show_all(window);

    g_timeout_add(1000, updateClock, (gpointer) piclock);
    gtk_widget_set_app_paintable(piclock, TRUE);
    gtk_widget_set_double_buffered(piclock, FALSE);

    gtk_main();
    piboxLoggerShutdown();
    updateSystemTime();

    return 0;
}
