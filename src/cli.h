/*******************************************************************************
 * PiBox service daemon
 *
 * cli.h:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef CLI_H
#define CLI_H

#include <gtk/gtk.h>
#include <pibox/parson.h>

/*========================================================================
 * Type definitions
 *=======================================================================*/
#define CLI_LOGTOFILE   0x00008    // Enable log to file
#define CLI_TEST        0x00020    // Enable test mode (read data files locally)
#define CLI_ROOT        0x00040    // Running as root

typedef struct _cli_t {
    int                 flags;      // Enable/disable features
    int                 verbose;    // Sets the verbosity level for the application
    char                *logFile;   // Name of local file to write log to
    char                *themeDir;  // Location where themes are installed
    char                *themeConf; // Name of conf file holding the name of the current theme
    JSON_Value          *theme;     // The current theme configuration, if any.
    GdkPixbuf           *face;      // Image surface for clock face, if any.
    GdkPixbuf           *hour;      // Image surface for hour hand, if any.
    GdkPixbuf           *min;       // Image surface for minute hand, if any.
    GdkPixbuf           *sec;       // Image surface for second hand, if any.
    GdkPixbuf           *overlay;   // Image surface for clock overlay, if any.
} CLI_T;


/*========================================================================
 * Text strings 
 *=======================================================================*/

/* Where are theme files stored at run time. */
#define THEMEDIR    "/etc/piclock/themes"
#define THEMEDIR_T  "data/themes"

/* What file holds the name of the current theme */
#define THEMECONF   "piclock.theme"

/* Version information should be passed from the build */
#ifndef VERSTR
#define VERSTR      "No Version String"
#endif

#ifndef VERDATE
#define VERDATE     "No Version Date"
#endif

#define CLIARGS     "Tl:v:"
#define USAGE \
"\n\
piclock [ -T | -l <filename> | -v <level> | -h? ]\n\
where\n\
\n\
    -T              Use test files (for debugging only) \n\
    -l filename     Enable local logging to named file \n\
    -v level        Enable verbose output: \n\
                    0: LOG_NONE  (default) \n\
                    1: LOG_INFO            \n\
                    2: LOG_WARN            \n\
                    3: LOG_ERROR           \n\
                    4: LOG_TRACE1          \n\
                    5: LOG_TRACE2          \n\
                    6: LOG_TRACE3          \n\
                    7: LOG_TRACE4          \n\
                    8: LOG_TRACE5          \n\
\n\
"


/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef CLI_C
CLI_T cliOptions;
#else
extern CLI_T cliOptions;
#endif /* CLI_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifdef CLI_H
void parseArgs(int argc, char **argv);
void initConfig( void );
void validateConfig( void );
int  isCLIFlagSet( int bits );
void setCLIFlag( int bits );
void unsetCLIFlag( int bits );
#else
extern void parseArgs(int argc, char **argv);
extern void initConfig( void );
extern void validateConfig( void );
extern int  isCLIFlagSet( int bits );
extern void setCLIFlag( int bits );
extern void unsetCLIFlag( int bits );
#endif

#endif /* !CLI_H */
