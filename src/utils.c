/*******************************************************************************
 * piclock
 *
 * utils.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define UTILS_C

#include <stdio.h>
#include <ctype.h>

#include "piclock.h"

/*
 *========================================================================
 * Name:   trim
 * Prototype:  char *trim( char * )
 *
 * Description:
 * Trim whitespace from a buffer by moving the provided pointer to the first
 * position that is not whitespace.
 *
 * Notes:
 * Assumes null terminated string as function argument.
 * Borrowed from StackOverflow:
 * http://stackoverflow.com/questions/122616/how-do-i-trim-leading-trailing-whitespace-in-a-standard-way
 *========================================================================
 */
char * 
trim( char *ptr )
{
    // Trim leading space
    while(isspace(*ptr)) 
        ptr++;
    return ptr;
}

/*
 *========================================================================
 * Name:   stripNewline
 * Prototype:  void *stripNewline( char * )
 *
 * Description:
 * Strip the newline from the end of the buffer.
 *
 * Notes:
 * Assumes null terminated string as function argument.
 *========================================================================
 */
void
stripNewline( char *buf )
{
    char *ptr = buf;
    while( (*ptr != '\n') && (*ptr != 0) ) 
        ptr++;
    if( *ptr == '\n' ) 
        *ptr = 0;
}

