/*******************************************************************************
 * PiBox service daemon
 *
 * utils.h:  Handle log messages
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef UTILS_H
#define UTILS_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef UTILS_C
extern int   getVideoLength( char *filename );
extern char *findVideo( char *filename );
extern char *trim( char *ptr );
extern int validIPAddress( char *ipaddr );
#endif /* !UTILS_C */
#endif /* !UTILS_H */
