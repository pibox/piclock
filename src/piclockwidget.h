/*******************************************************************************
 * piclock
 *
 * piclockwidget.h:  custom analog clock widget
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef __PICLOCKWIDGET_H
#define __PICLOCKWIDGET_H

#include <gtk/gtk.h>
#include <cairo.h>

G_BEGIN_DECLS

#define GTK_PICLOCK(obj) GTK_CHECK_CAST(obj, gtk_piclock_get_type (), GtkPiclock)
#define GTK_PICLOCK_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gtk_piclock_get_type(), GtkPiclockClass)
#define GTK_IS_PICLOCK(obj) GTK_CHECK_TYPE(obj, gtk_piclock_get_type())

typedef struct _GtkPiclock GtkPiclock;
typedef struct _GtkPiclockClass GtkPiclockClass;

typedef struct _center_pt_t {
    gint    x;
    gint    y;
} CENTER_PT_T;

typedef struct _piclock_theme_t {
    char            *face;          // Name of clock face file.
    char            *hour;          // Name of hour hand file.
    char            *min;           // Name of minute hand file.
    char            *sec;           // Name of second hand file.
    char            *overlay;       // Name of clock overylay file.
    CENTER_PT_T     hour_pt;        // Center point of hour hand.
    CENTER_PT_T     min_pt;         // Center point of minute hand.
    CENTER_PT_T     sec_pt;         // Center point of second hand.
    // cairo_surface_t *face_sf;    // Image surface for clock face, if any.
    // cairo_surface_t *hour_sf;    // Image surface for hour hand, if any.
    // cairo_surface_t *min_sf;     // Image surface for minute hand, if any.
    // cairo_surface_t *sec_sf;     // Image surface for second hand, if any.
    // cairo_surface_t *overlay_sf; // Image surface for clock overlay, if any.
    GdkPixbuf       *face_sf;       // Image surface for clock face, if any.
    GdkPixbuf       *hour_sf;       // Image surface for hour hand, if any.
    GdkPixbuf       *min_sf;        // Image surface for minute hand, if any.
    GdkPixbuf       *sec_sf;        // Image surface for second hand, if any.
    GdkPixbuf       *overlay_sf;    // Image surface for clock overlay, if any.
    char            face_sf_scaled; // Image surface for clock face, if any.
} PICLOCK_THEME_T;

struct _GtkPiclock {
  GtkDrawingArea parent;
  GdkPixmap *pixmap;
  GdkColor color;
  gint hoursHand;
  gint minutesHand;
  gint secondsHand;
  PICLOCK_THEME_T *theme;
  gfloat scale_w_percentage;
  gfloat scale_h_percentage;
};

struct _GtkPiclockClass {
  GtkDrawingAreaClass parent_class;
};

/* Prototypes */
GtkType gtk_piclock_get_type(void);
void gtk_piclock_tick(GtkPiclock*);
GtkWidget *gtk_piclock_new();
void gtk_piclock_set_time(GtkPiclock *piclock, gint hour, gint min);
void gtk_piclock_set_theme(GtkPiclock *piclock, PICLOCK_THEME_T *ptr);

G_END_DECLS

#endif /* __PICLOCKWIDGET_H */
