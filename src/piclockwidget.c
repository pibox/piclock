/*******************************************************************************
 * piclock
 *
 * piclockwidget.c:  custom analog clock widget
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 * 
 * Based on:
 * Custom GTK+ widget - http://zetcode.com/tutorials/gtktutorial/customwidget/
 * Clock Widget - https://github.com/humbhenri/clocks/tree/master/gtk-clock
 ******************************************************************************/
#define PICLOCKWIDGET_C

#include <time.h>
#include <math.h>
#include <gtk/gtk.h>
#include <pibox/log.h>
#include "piclockwidget.h"

#define RADIAN(x) (((x) * M_PI/30.0) - M_PI/2.0)
#define HAND_HOUR   1
#define HAND_MIN    2
#define HAND_SEC    3

static void gtk_piclock_class_init(GtkPiclockClass *klass);
static void gtk_piclock_init(GtkPiclock *piclock);
static void gtk_piclock_size_request(GtkWidget *widget, GtkRequisition *requisition);
static void gtk_piclock_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static void gtk_piclock_realize(GtkWidget *widget);
static gboolean gtk_piclock_expose(GtkWidget *widget, GdkEventExpose *event);
static void gtk_piclock_paint(GtkWidget *widget);
static void gtk_piclock_destroy(GtkObject *object);

GtkType
gtk_piclock_get_type(void)
{
    static GtkType gtk_piclock_type = 0;
    if (!gtk_piclock_type) {
        static const GtkTypeInfo gtk_piclock_info = {
            "GtkPiclock",
            sizeof(GtkPiclock),
            sizeof(GtkPiclockClass),
            (GtkClassInitFunc) gtk_piclock_class_init,
            (GtkObjectInitFunc) gtk_piclock_init,
            NULL,
            NULL,
            (GtkClassInitFunc) NULL
        };
        gtk_piclock_type = gtk_type_unique(GTK_TYPE_WIDGET, &gtk_piclock_info);
    }
    return gtk_piclock_type;
}

GtkWidget * gtk_piclock_new()
{
    return GTK_WIDGET(gtk_type_new(gtk_piclock_get_type()));
}

static void
gtk_piclock_class_init(GtkPiclockClass *klass)
{
    GtkWidgetClass *widget_class;
    GtkObjectClass *object_class;

    widget_class = (GtkWidgetClass *) klass;
    object_class = (GtkObjectClass *) klass;

    widget_class->realize = gtk_piclock_realize;
    widget_class->size_request = gtk_piclock_size_request;
    widget_class->size_allocate = gtk_piclock_size_allocate;
    widget_class->expose_event = gtk_piclock_expose;

    object_class->destroy = gtk_piclock_destroy;
}

static void
gtk_piclock_init(GtkPiclock *piclock)
{
    time_t now;
    struct tm* timeinfo;
    time(&now);
    timeinfo = localtime(&now);
    piclock->hoursHand = (timeinfo->tm_hour%12) * 5;
    piclock->minutesHand = timeinfo->tm_min;
    piclock->secondsHand = timeinfo->tm_sec;
    piclock->pixmap = NULL;
    piclock->theme = NULL;
}

static void
gtk_piclock_size_request(GtkWidget *widget, GtkRequisition *requisition)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_PICLOCK(widget));
    g_return_if_fail(requisition != NULL);
}

static void
gtk_piclock_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_PICLOCK(widget));
    g_return_if_fail(allocation != NULL);

    widget->allocation = *allocation;
    if (GTK_WIDGET_REALIZED(widget)) {
        gdk_window_move_resize(
            widget->window,
            allocation->x, allocation->y,
            allocation->width, allocation->height
        );
    }
}

static void
gtk_piclock_realize(GtkWidget *widget)
{
    GdkWindowAttr attributes;
    guint attributes_mask;
    GtkStyle *style;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_PICLOCK(widget));

    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x = widget->allocation.x;
    attributes.y = widget->allocation.y;
    attributes.wclass = GDK_INPUT_OUTPUT;
    attributes.event_mask = gtk_widget_get_events(widget) | GDK_EXPOSURE_MASK;
    attributes_mask = GDK_WA_X | GDK_WA_Y;

    widget->window = gdk_window_new(
        gtk_widget_get_parent_window (widget),
        & attributes, attributes_mask
    );

    style = gtk_widget_get_style (widget);
    if (style != NULL) 
        GTK_PICLOCK(widget)->color = style->bg[GTK_STATE_NORMAL];

    gdk_window_set_user_data(widget->window, widget);
    widget->style = gtk_style_attach(widget->style, widget->window);
    gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);
}

static gboolean
gtk_piclock_expose(GtkWidget *widget, GdkEventExpose *event)
{
    int width, height;

    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_PICLOCK(widget), FALSE);
    g_return_val_if_fail(event != NULL, FALSE);

    // Copy the entire pixmap over the widget's drawing area.
    if ( GTK_PICLOCK(widget)->pixmap != NULL )
    {
        piboxLogger(LOG_INFO, "widget->w/h: %d/%d\n", widget->allocation.width, widget->allocation.height);
        gdk_drawable_get_size(GTK_PICLOCK(widget)->pixmap, &width, &height);
        piboxLogger(LOG_INFO, "pixmap w/h: %d/%d\n", width, height);

        gdk_draw_drawable(widget->window,
            widget->style->fg_gc[GTK_WIDGET_STATE(widget)], GTK_PICLOCK(widget)->pixmap,
            event->area.x, event->area.y,
            event->area.x, event->area.y,
            event->area.width, event->area.height);
    }
    return FALSE;
}

static double 
deg2rad( double degrees )
{
    return((double)((double)degrees * ( (double)M_PI/(double)180.0 ))); 
}

/*
 * Draw the face on the cairo drawing context.
 */
static void
draw_face(GtkWidget *widget, cairo_t *cr)
{
    GdkPixbuf       *image = NULL;
    GdkPixbuf       *scaledImage = NULL;
    gint            width, height;
    GtkPiclock      *piclock;
    GtkRequisition  req;

    piclock = GTK_PICLOCK(widget);
    image = piclock->theme->face_sf;
    if ( image == NULL )
        return;

    /*
     * Get widget area.
     */
    req.width = gdk_window_get_width(widget->window);
    req.height = gdk_window_get_height(widget->window);
    piboxLogger(LOG_INFO, "widget w/h: %d %d\n", req.width, req.height);

    /* Get the image width/height. Note: only used for debugging. */
    width = gdk_pixbuf_get_width(image);
    height = gdk_pixbuf_get_height(image);
    piboxLogger(LOG_INFO, "image w/h: %d %d\n", width, height);

    /*
     * Scale the face to fit.  We need to do this every time in case the widget changes size.
     */
    scaledImage = gdk_pixbuf_scale_simple(image, req.width, req.height, GDK_INTERP_BILINEAR);
    piboxLogger(LOG_INFO, "scaled image w/h: %d %d\n", req.width, req.height);

    /* Scale percentage - used by later by the hands drawing. */
    piclock->scale_w_percentage = (gfloat)((gfloat)req.width/(gfloat)width);
    piclock->scale_h_percentage = (gfloat)((gfloat)req.height/(gfloat)height);
    piboxLogger(LOG_INFO, "scaling w/h: %f %f\n", piclock->scale_w_percentage, piclock->scale_h_percentage);

    /* Draw the face on the Cairo surface. */
    gdk_cairo_set_source_pixbuf(cr, scaledImage, 0, 0);
    cairo_paint(cr);

    /* Free the scaled image. */
    g_object_unref(scaledImage);
}

/*
 * Draw the clock overlay on the cairo drawing context.
 */
static void
draw_overlay(GtkWidget *widget, cairo_t *cr)
{
    GdkPixbuf       *image = NULL;
    GdkPixbuf       *scaledImage = NULL;
    gint            width, height;
    GtkPiclock      *piclock;
    GtkRequisition  req;

    piclock = GTK_PICLOCK(widget);

    image = piclock->theme->overlay_sf;
    if ( image == NULL )
        return;

    /*
     * Get widget area.
     */
    req.width = gdk_window_get_width(widget->window);
    req.height = gdk_window_get_height(widget->window);
    piboxLogger(LOG_INFO, "widget w/h: %d %d\n", req.width, req.height);

    /* Get the image width/height. Note: only used for debugging. */
    image = piclock->theme->overlay_sf;
    width = gdk_pixbuf_get_width(image);
    height = gdk_pixbuf_get_height(image);
    piboxLogger(LOG_INFO, "image w/h: %d %d\n", width, height);

    /*
     * Scale the face to fit.  We need to do this every time in case the widget changes size.
     */
    scaledImage = gdk_pixbuf_scale_simple(image, req.width, req.height, GDK_INTERP_BILINEAR);
    piboxLogger(LOG_INFO, "scaled image w/h: %d %d\n", req.width, req.height);

    /* Draw the face on the Cairo surface. */
    gdk_cairo_set_source_pixbuf(cr, scaledImage, 0, 0);
    cairo_paint(cr);
}

/*
 * Draw the hands on the cairo drawing context.
 */
static void
draw_hand(GtkWidget *widget, cairo_t *cr, gint hand)
{
    GdkPixbuf       *image = NULL;
    cairo_t         *hand_cr;
    cairo_surface_t *surface;
    cairo_pattern_t *pattern;
    cairo_format_t  format;
    gint            width, height;
    gdouble         offset_x, offset_y;
    gdouble         degrees;
    GtkPiclock      *piclock;
    GtkRequisition  req;
    cairo_matrix_t  matrix;

    piclock = GTK_PICLOCK(widget);
    req.width = gdk_window_get_width(widget->window);
    req.height = gdk_window_get_height(widget->window);

    /*
     * Setup based on the hand requested.
     */
    switch( hand )
    {
        case HAND_HOUR:
            piboxLogger(LOG_TRACE1, "Drawing Hour Hand.\n");
            image = piclock->theme->hour_sf;
            degrees = -1*(double)piclock->hoursHand/60.0*360.0;
            offset_x = (double)piclock->theme->hour_pt.x;
            offset_y = (double)piclock->theme->hour_pt.y;
            break;
        case HAND_MIN:
            piboxLogger(LOG_TRACE1, "Drawing Minute Hand.\n");
            image = piclock->theme->min_sf;
            degrees = -1*(double)piclock->minutesHand/60.0*360.0;
            offset_x = (double)piclock->theme->min_pt.x;
            offset_y = (double)piclock->theme->min_pt.y;
            break;
        case HAND_SEC:
            piboxLogger(LOG_TRACE1, "Drawing Second Hand.\n");
            image = piclock->theme->sec_sf;
            degrees = -1*(double)piclock->secondsHand/60.0*360.0;
            offset_x = (double)piclock->theme->sec_pt.x;
            offset_y = (double)piclock->theme->sec_pt.y;
            break;
    }
    if ( image == NULL )
        return;
    piboxLogger(LOG_TRACE1, "Have a hand image - ready to draw.\n");
    width = gdk_pixbuf_get_width(image);
    height = gdk_pixbuf_get_height(image);

    /* Generate a pattern (mask) for the hand. */
    format = (gdk_pixbuf_get_has_alpha (image)) ? CAIRO_FORMAT_ARGB32 : CAIRO_FORMAT_RGB24;
    surface = cairo_image_surface_create (format, width, height);
    hand_cr = cairo_create (surface);
    pattern = cairo_pattern_create_for_surface (surface);
    gdk_cairo_set_source_pixbuf (hand_cr, image, 0, 0);
    cairo_scale(hand_cr, piclock->scale_w_percentage, piclock->scale_h_percentage);
    cairo_paint (hand_cr);

    /* translate the rotation point of the hand to the origin. */
    cairo_matrix_init_identity (&matrix);
    cairo_matrix_translate (&matrix, (double)offset_x, (double)offset_y);

    /* Rotate to the correct clock position. */
    piboxLogger(LOG_TRACE1, "Hand degrees: %f\n", degrees);
    cairo_matrix_rotate (&matrix, deg2rad(degrees));

    /* Move back to the original position */
    piboxLogger(LOG_TRACE1, "req w/h: %d %d\n", req.width, req.height);
    piboxLogger(LOG_TRACE1, "pos x/y: %f %f\n", -offset_x, -offset_y);

    /* Move to the center of the clock window */
    cairo_matrix_translate (&matrix, 
            -req.width/2,
            -req.height/2);
    cairo_pattern_set_matrix (pattern, &matrix);

    cairo_set_source (cr, pattern);
    cairo_paint(cr);
    cairo_pattern_destroy (pattern);

    /* Is this necessary? */
    cairo_surface_destroy(surface);
    cairo_destroy(hand_cr);
}

/*
 * Update the pixmap buffer that will be copied into the widget
 * window.
 */
static void
gtk_piclock_paint(GtkWidget *widget)
{
    cairo_t *cr;
    double x, y, radius;
    double hoursHandSize, minutesHandSize, secondsHandSize;
    int width, height;
    GdkPixmap *pixmap;
    GtkPiclock      *piclock;

    piclock = GTK_PICLOCK(widget);
    x = widget->allocation.x + widget->allocation.width / 2;
    y = widget->allocation.y + widget->allocation.height / 2;

    piboxLogger(LOG_INFO, "allocation x/y %d/%d\n", widget->allocation.x, widget->allocation.y);
    piboxLogger(LOG_INFO, "allocation w/h %d/%d\n", widget->allocation.width, widget->allocation.height);
    piboxLogger(LOG_INFO, "x/y: %f/%f\n", x, y);

    radius = MIN (widget->allocation.width / 2, widget->allocation.height / 2) *0.5;
    hoursHandSize = 0.25 * radius;
    minutesHandSize = 0.5 * radius;
    secondsHandSize = 0.75 * radius;

    piboxLogger(LOG_INFO, "radius, hour, minute, sec: %f, %f, %f, %f \n", 
            radius, hoursHandSize, minutesHandSize, secondsHandSize);

    if ( GTK_PICLOCK(widget)->pixmap == NULL )
    {
        piboxLogger(LOG_INFO, "Allocating pixmap\n");
        GTK_PICLOCK(widget)->pixmap = 
            gdk_pixmap_new(widget->window,widget->allocation.width,widget->allocation.height,-1);
    }
    pixmap = GTK_PICLOCK(widget)->pixmap;

    //create a gtk-independant surface to draw on
    gdk_drawable_get_size(pixmap, &width, &height);
    piboxLogger(LOG_INFO, "pixmap w/h: %d/%d\n", width, height);
    cairo_surface_t *cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    cr = cairo_create(cst);

    // Fill with widget background color first.  This matches our current style.
    cairo_set_source_rgb(cr, 
        (float)(GTK_PICLOCK(widget)->color.red)/65535,
        (float)(GTK_PICLOCK(widget)->color.green)/65535,
        (float)(GTK_PICLOCK(widget)->color.blue)/65535
    );
    cairo_rectangle(cr, 0, 0, width, height);
    cairo_fill(cr);

    if ( piclock->theme == NULL )
    {
        radius = MIN (widget->allocation.width / 2, widget->allocation.height / 2) *0.5;
    }
    else
    {
        int face_width, face_height;
        // face_width  = cairo_image_surface_get_width(piclock->theme->face_sf);
        // face_height = cairo_image_surface_get_height(piclock->theme->face_sf);
        face_width  = gdk_pixbuf_get_width(piclock->theme->face_sf);
        face_height = gdk_pixbuf_get_height(piclock->theme->face_sf);
        radius = MIN (face_width, face_height) *0.5;
        piboxLogger(LOG_INFO, "radius face w/h: %d %d\n", face_width, face_height);
    }
    piboxLogger(LOG_INFO, "radius: %f\n", radius);
    hoursHandSize = 0.65 * radius;
    minutesHandSize = 0.85 * radius;
    secondsHandSize = 0.98 * radius;

    /*
     * If a theme was provided, use it.
     */
    if ( piclock->theme != NULL )
    {
        draw_face(widget, cr);
        draw_hand(widget, cr, HAND_HOUR);
        draw_hand(widget, cr, HAND_MIN);
        draw_hand(widget, cr, HAND_SEC);
        draw_overlay(widget, cr);
        goto this_exit;
    }

    /*
     * Default with no theme is a simple clock with no face.
     */

    /* draw circle under bg image */
    cairo_arc(cr, x, y, radius+1, 0, 2*M_PI);
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_fill_preserve(cr);
    cairo_stroke(cr);

    /* draw circle under bg image */
    cairo_set_line_width(cr, .5 * cairo_get_line_width(cr));
    cairo_arc(cr, x, y, radius+0.5, 0, 2*M_PI);

    /* draw hands */
    /* Hour hand */
    cairo_set_line_width(cr, 2 * cairo_get_line_width(cr));
    cairo_set_source_rgb(cr, 0, 255, 0);
    cairo_move_to(cr, x, y);
    cairo_line_to(cr, x + hoursHandSize * cos(RADIAN(GTK_PICLOCK(widget)->hoursHand)), 
            y + hoursHandSize * sin(RADIAN(GTK_PICLOCK(widget)->hoursHand)));
    cairo_stroke(cr);

    /* Minute hand */
    cairo_move_to(cr, x, y);
    cairo_line_to(cr, x + minutesHandSize * cos(RADIAN(GTK_PICLOCK(widget)->minutesHand)), 
            y + minutesHandSize * sin(RADIAN(GTK_PICLOCK(widget)->minutesHand)));
    cairo_stroke(cr);

    /* Second hand */
    cairo_set_source_rgb(cr, 255, 0, 0);
    cairo_set_line_width(cr, 0.25 * cairo_get_line_width(cr));
    cairo_move_to(cr, x, y);
    cairo_line_to(cr, x + secondsHandSize * cos(RADIAN(GTK_PICLOCK(widget)->secondsHand)), 
            y + secondsHandSize * sin(RADIAN(GTK_PICLOCK(widget)->secondsHand)));   
    cairo_stroke(cr);

this_exit:
    cairo_destroy(cr);

    cairo_t *cr_pixmap = gdk_cairo_create(pixmap);
    cairo_set_source_surface (cr_pixmap, cst, 0, 0);
    cairo_paint(cr_pixmap);
    cairo_destroy(cr_pixmap);
    cairo_surface_destroy(cst);
}

static void
gtk_piclock_destroy(GtkObject *object)
{
    GtkPiclockClass *klass;

    g_return_if_fail(object != NULL);
    g_return_if_fail(GTK_IS_PICLOCK(object));

    klass = gtk_type_class(gtk_widget_get_type());
    if (GTK_OBJECT_CLASS(klass)->destroy) {
        (* GTK_OBJECT_CLASS(klass)->destroy) (object);
    }
}

/* 
 * Update clock time with one second passes
 */
void 
gtk_piclock_tick(GtkPiclock *clock)
{
    GdkRegion *region;

    clock->secondsHand = (clock->secondsHand + 1) % 60;
    if (clock->secondsHand == 0) {
        clock->minutesHand = (clock->minutesHand + 1) % 60;
        if (clock->minutesHand == 0) {
            clock->hoursHand = (clock->hoursHand + 5) % 60;
        }
    }
    gtk_piclock_paint(GTK_WIDGET(clock));
    region = gdk_drawable_get_clip_region(GTK_WIDGET(clock)->window);
    gdk_window_invalidate_region(GTK_WIDGET(clock)->window, region, TRUE);
    gdk_window_process_updates(GTK_WIDGET(clock)->window, TRUE);
}

void
gtk_piclock_set_time(GtkPiclock *piclock, gint hour, gint min)
{
    piclock->secondsHand = 0;
    piclock->minutesHand = min;
    piclock->hoursHand = (hour%12) * 5;
    gtk_piclock_paint(GTK_WIDGET(piclock));
}

void
gtk_piclock_set_theme(GtkPiclock *piclock, PICLOCK_THEME_T *ptr)
{
    piclock->theme = ptr;
}
